-- An implementation of a Hilbert R-tree, as described in the paper located at
-- <http://www.cis.temple.edu/~vasilis/Courses/CIS750/Papers/Hilbert
-- Rtree-Kamel.pdf>

module HilbertRTree where

import Data.Maybe
import Data.Bits

--a point in R2, with coordinates as doubles
data Pt = Pt Int Int deriving (Eq, Show)

--a minimum bounding rectangle first point is bottom left, second is top right
data Rect = Rect Pt Pt deriving (Eq, Show)

--a node or leaf of the RTree
data RTree a = Leaf [(Rect, a)]
             | Node [(Rect, RTree a, Int)]
               deriving Show

--number of rectangles in a leaf or children of a node respectively
--chosen arbitrarily
c_l = 10
c_n = 4

center :: Rect -> Pt
center (Rect (Pt x1 y1) (Pt x2 y2)) = Pt (avg x1 x2) (avg y1 y2)
  where avg a b = (a + b) `div` 2
  
-- two rectangles r1 and r2 intersect if their projections onto both axes
-- intersect
intersects :: Rect -> Rect -> Bool
intersects r1 r2 = xp r1 `intersect` xp r2 && yp r1 `intersect` yp r2
  where
    xp (Rect (Pt x1 _) (Pt x2 _)) = (x1, x2)
    yp (Rect (Pt _ y1) (Pt _ y2)) = (y1, y2)
    (intersect) (a, b) (c, d) = b >= c && a <= d
 
inRect :: Pt -> Rect -> Bool
inRect (Pt x y) (Rect (Pt x1 y1) (Pt x2 y2)) =
  x1 <= x &&
  y1 >= y &&
  x2 >= x &&
  y2 <= y
  
emptyTree :: RTree a
emptyTree = Leaf []
  
searchTree :: RTree a -> Rect -> [a]
searchTree (Leaf l) w = mapMaybe grabIntersecting l
  where grabIntersecting (r, a)
          | r `intersects` w = Just a
          | otherwise = Nothing
searchTree (Node l) w = concat $ mapMaybe recur l
  where recur (r, n, _)
          | r `intersects` w = Just $ searchTree n w
          | otherwise = Nothing
                        
-- some serious fucking coupling here - we pass garbage to insert' and depend on
-- the fact that it doesn't look at the old mbr or old h to make the new root
insert :: RTree a -> Rect -> a -> RTree a
insert t r a = grow $ snd3 $ insert' (undefined, t, undefined) r a
  where
    grow n@(Leaf l) | length l <= c_l = n
                    | otherwise = Node $ rebalanceLeaves [(undefined, n, undefined)]
    grow n@(Node l) | length l <= c_n = n
                    | otherwise = Node $ rebalanceNodes [(undefined, n, undefined)]

insert' :: (Rect, RTree a, Int) -> Rect -> a -> (Rect, RTree a, Int)
insert' (r0, (Leaf l), h0) r a =
  let (h, t) = halve ((hilbert r <) . hilbert . fst) l in
  (bound r r0, Leaf $ h ++ [(r, a)] ++ t,
   (if length t == 0 then hilbert r else h0))
insert' (r0, (Node l), h0) r a = (bound r0 r, nn, max h0 $ hilbert r)
  where
    nn =
      case insert' cur r a of
        result@(_, (Leaf l), _) ->
          if length l <= c_l 
          then Node $ prev ++ [result] ++ rest
          else Node $ rebalanceLeaves (prev ++ [result] ++ rest)
        result@(_, (Node l), _) ->
          if length l <= c_n
          then Node $ prev ++ [result] ++ rest
          else Node $ rebalanceNodes (prev ++ [result] ++ rest)
    (prev, cur, rest) =
      case halve ((hilbert r <) . hilbert . fst3) l of
        ([], []) -> error "empty non-leaf node"
        (h, t:ts) -> (h, t, ts)
        (h, []) ->
          let (last, rest) = (\ (x:xs) -> (x, reverse xs)) $ reverse h in
          (rest, last, [])

fst3 :: (a, b, c) -> a
fst3 (a, b, c) = a

snd3 :: (a, b, c) -> b
snd3 (a, b, c) = b

-- note that RL does not use the MBRs or cached max hilbert values for the old
-- leaves, but recalculates everything instead
rebalanceLeaves :: [(Rect, RTree a, Int)] -> [(Rect, RTree a, Int)]
rebalanceLeaves l =
  let all_entries = foldl (++) [] $ map (\(Leaf l') -> l') $ map snd3 l in
  let cnt = length l in
  let sub_len = length all_entries `div` cnt in
  let cnt' = if sub_len >= c_l then cnt + 1 else cnt in
  let sub_len' = length all_entries `div` cnt' in
  map
  (\lst -> (foldl1 bound $ map fst lst, Leaf lst, getMaxHilbert lst))
  (even_split sub_len' all_entries)
  where
    even_split _ [] = []
    even_split cnt lst = (take cnt lst):(even_split cnt $ drop cnt lst)
    
-- get the highest h value in a list of (rect, tree, h) tuples
-- by construction, this is the last one
getHighest :: [(a, b, c)] -> c
getHighest = (\(a, b, c) -> c) . last

getMaxHilbert :: [(Rect, a)] -> Int
getMaxHilbert l = foldl max 0 $ map (hilbert . fst) l
    
rebalanceNodes :: [(Rect, RTree a, Int)] -> [(Rect, RTree a, Int)]
rebalanceNodes l =
  let all_entries = foldl (++) [] $ map (\(Node l') -> l') $ map snd3 l in
  let cnt = length l in
  let sub_len = length all_entries `div` cnt in
  let cnt' = if sub_len >= c_l then cnt + 1 else cnt in
  let sub_len' = length all_entries `div` cnt' in
  map
  (\l -> (foldl1 bound $ map fst3 l, Node l, getHighest l))
  (even_split sub_len' all_entries)
  where 
    even_split _ [] = []
    even_split cnt l = (take cnt l):(even_split cnt $ drop cnt l)
    
-- finds the minimum bounding rectangle of the two rectangles provided
bound :: Rect -> Rect -> Rect
bound (Rect (Pt x1 y1) (Pt x2 y2)) (Rect (Pt x1' y1') (Pt x2' y2')) =
  Rect (Pt (min x1 x1') (min y1 y1')) (Pt (max x2 x2') (max y2 y2'))
    
-- splits a list just before the first element that satisfies the test
halve :: (a -> Bool) -> [a] -> ([a], [a])
halve test (x:xs)
  | not $ test x = let (h, t) = halve test xs in (x:h, t)
  | test x = ([], x:xs)
halve _ [] = ([], [])
                
hilbert :: Rect -> Int
hilbert = xy2d . center

-- hilbert curve conversions adapted from:
-- <http://en.wikipedia.org/wiki/Hilbert_curve>

width :: Int
width = 2^16

xy2d :: Pt -> Int
xy2d (Pt x y) = loop x y (width `div` 2) 0
  where
    loop _ _ 0 d = d
    loop x y s d =
      let (rx, ry) = (sharebits x s, sharebits y s) in
      let (x', y') = rot s x y rx ry in
      loop x' y' (s `div` 2) (d+s*s*((3*rx) `xor` ry))
    sharebits a b = signum $ a .&. b

d2xy :: Int -> Pt
d2xy d = loop 1 d 0 0
  where loop s t x y
          | s == width = Pt x y 
          | otherwise =
            let (rx, ry) = (1 .&. (t `div` 2), 1 .&. (t `xor` rx)) in
            let (x', y') = rot s x y rx ry in
            loop (2*s) (t `div` 4) (x' + s * rx) (y' + s * ry)
            
rot :: Int -> Int -> Int -> Int -> Int -> (Int, Int)
rot s x y rx ry
  | ry == 1 = (x, y)
  | ry == 0 && rx == 0 = (y, x)
  | ry == 0 && rx == 1 = (s - 1 - y, s - 1 - x)