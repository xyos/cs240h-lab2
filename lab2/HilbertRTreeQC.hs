import Test.QuickCheck
import HilbertRTree

newtype InRange = InRange Int deriving Show

instance Arbitrary InRange where
  arbitrary = do i <- arbitrary; return $ InRange $ i `posmod` (width^2)
    where a `posmod` b
            | a < 0 = (a `mod` b) + b
            | otherwise = a `mod` b

t_inverses (InRange d) = d == (xy2d $ d2xy d)