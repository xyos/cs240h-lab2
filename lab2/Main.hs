import HilbertRTree
import System.Environment
import Data.List hiding (insert)
import System.CPUTime
import Data.Maybe

main :: IO ()
main = do
  args <- getArgs
  file <- if length args == 1
          then return $ head args
          else fail "please pass the rectangles file as the only argument"
  (t, (tree, cnt)) <- time $ file_loop file
  putStrLn $ file ++ ": " ++ show cnt ++ " rectangles read in " ++
    (show t) ++ " milliseconds"
  query_loop tree

file_loop :: String -> IO (RTree Rect, Int)
file_loop filename = do 
  text <- (readFile filename)
  let rects = mapMaybe toRect $ lines text in 
    let cnt = length rects in
    return (foldl myinsert emptyTree rects, cnt)
  where
    myinsert t r = insert t r r

query_loop :: RTree Rect -> IO ()
query_loop tree = do
  line <- getLine
  case toRect line of
    Nothing -> do
      if line == "quit"
        then return ()
        else do {putStrLn problem; query_loop tree}
    Just r -> do      
      (t, matches) <- time $ return $! searchTree tree r
      putStrLn $ "found " ++ (show . length $ matches) ++ " matches in "
        ++ show t ++ " milliseconds:"
      --there has to be an easier shortcut for this, right?
      mapM_ (\r -> putStrLn $ show r) (take 4 matches)
      query_loop tree

toRect :: String -> Maybe Rect
toRect str = 
  let parts = map read $ split str ',' in
  case parts of
    [x1, y2, x1', y1, x2, y1', x2', y2'] ->
      Just (bound
            (Rect (Pt x1 y1) (Pt x2 y2))
            (Rect (Pt x1' y1') (Pt x2' y2')))
    _ -> Nothing
            
time :: IO a -> IO (Int, a)
time action = do
  start <- getCPUTime
  val <- action
  end <- getCPUTime
  return (fromIntegral $ end - start, val)
  
problem :: String
problem = "please enter a rectangle as a comma-separated list of alternating x \ 
\and y coordinates, or \"quit\" to quit"

--why isn't this in the standard library?
--note to self - a string ending in c does not get an empty list at the end.
split :: String -> Char -> [String]
split s c = case splitFirst s c [] of
  (_, []) -> [s]
  (h, t) -> h:(split t c)
  where
    splitFirst (x:xs) c acc
      | x == c = (reverse acc, xs)
      | otherwise = splitFirst xs c (x:acc)
    splitFirst [] _ acc = (acc, [])
  
